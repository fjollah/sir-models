from HelperClass import HelperClass
import random as rd
import networkx as nx
import numpy as np


TYPE = "infection_type"
SUSCEPTIBLE = 'S'
INFECTED = 'I'
RECOVERED = 'R'

color_map_dict = {INFECTED: 'red', SUSCEPTIBLE: 'blue', RECOVERED: 'green'}

#TODO Student set the your student id without leading 0
student_id = 11831695
helper = HelperClass()


# Resets all nodes to susceptible.
# params
# graph: networkx graph
def resetTypes(graph):
    for node in graph.nodes:
        graph.nodes[node][TYPE] = SUSCEPTIBLE

# Counts the number of susceptibles, infected and recovered in a
# given networkx graph
# params
# graph: networkx graph
# return value
# tuple (susceptibles, infected, recovered)
# where e.g. susceptibles contains the number of susceptibles in
# the given graph
def countTypes(graph):
    sus = []
    for (p, d) in graph.nodes(data=True):
        if d['infection_type'] == 'S':
            sus.append(p)
    inf = []
    for (p, d) in graph.nodes(data=True):
        if d['infection_type'] == 'I':
            inf.append(p)
    rec = []
    for (p, d) in graph.nodes(data=True):
        if d['infection_type'] == 'R':
            rec.append(p)

    susceptible = len(sus)
    infected = len(inf)
    recovered = len(rec)

    # TODO Student: implement

    return susceptible, infected, recovered

# getStdAndMean calculates the mean and the standard deviation of a
# given state list.
# params
# state_list: list containing e.g. the number of susceptibles for all epochs
# returns a tuple (mean, std)
# where mean and std are the mean and the standard deviation for the corresponding
# time step over all epochs
def getStdAndMean(state_list):
   
    mean, std = [0.0] * 100, [0.0] * 100

    mean, std=np.mean(state_list,axis=0),np.std(state_list,axis=0)

    return mean, std

# SIR function calculates the spread of a disease based on the given
# infection and recovery rate.
# params
# graph: networkx Graph on which the spread of a disease should be calculated
# infection_rate (beta): the probability that a node gets infected when a neighbor is infected
# recovery_rate (gamma): the probability that a infected node recovers from the disease
# iterations: number of iterations
# returns tuple (susceptible, infected, recovered)
# where each variable of the tuple is a list containing the number of
# e.g. susceptibles at a certain time step
def SIR(graph, infection_rate=0.1, recovery_rate=0.05, iterations=10000):

    # TODO Student: Implement a SIR Model based on the infection rate and the
    # recovery rate given. At each time step one node should be updated.
    # Every 100th time step a screenshot from the network shoot be taken
    # and the values should be saved in the corresponding
    # lists (susceptibles, infected, recovered)
    # At the beginning one graph node should be chosen on random
    # which is infected
    
    nr_nodes = len(graph.nodes)
    susceptibles=[]
    infected=[]
    recovered=[]
    rand_node = np.random.choice(graph.nodes())

    I0=rand_node
    graph.nodes[I0][TYPE] = INFECTED

    S0,I0,R0=countTypes(graph)
  
    infected.append(I0)
    susceptibles.append(S0)
    recovered.append(R0)
   
    for i in range(0,iterations):
        
        #pick random node to update from list of nodes
        up_node = np.random.choice(graph.nodes())
        neighbors_list=list(graph.neighbors(up_node))


        if(graph.nodes[up_node][TYPE] == SUSCEPTIBLE):
            for node in neighbors_list:
                if(graph.nodes[node][TYPE]==INFECTED and rd.random()<infection_rate):
                    graph.nodes[up_node][TYPE]=INFECTED
        elif(graph.nodes[up_node][TYPE] == INFECTED):
            if(rd.random()<recovery_rate):
                graph.nodes[up_node][TYPE]=RECOVERED

        if i%100==0:
            S,I,R=countTypes(graph)
            susceptibles.append(S)
            infected.append(I)
            recovered.append(R)

            
    color_map = [color_map_dict[SUSCEPTIBLE] for _ in
                 range(nr_nodes)]
    return susceptibles, infected, recovered


if __name__ == u'__main__':
    graph1, graph2 = helper.generateGraph(seed=student_id)
    graphs = [graph1, graph2]
    epochs = 15
    gid = 0
    for graph in graphs:
        gid = gid+1
        whole_sus = []
        whole_inf = []
        whole_rec = []
        for i in range(epochs):
            resetTypes(graph)
            sus, inf, rec = SIR(graph)

            whole_sus.append(sus)
            whole_inf.append(inf)
            whole_rec.append(rec)

        mean_sus, std_sus = getStdAndMean(whole_sus)
        mean_inf, std_inf = getStdAndMean(whole_inf)
        mean_rec, std_rec = getStdAndMean(whole_rec)

        infection = [mean_sus, mean_inf, mean_rec]
        std = [std_sus, std_inf, std_rec]

        x_axis = np.linspace(0, 10000, len(mean_sus))
        y_axis_labels = ["Susceptible", "Infected", "Recovered"]

        helper.plotInfection(infection, x_axis, y_axis_labels, std,
                             x_label="Time(Iteration)",
                             y_label="Number Nodes",
                             title="Infection Spread",
                             filename="SIR_" + str(gid))
