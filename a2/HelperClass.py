import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
import seaborn as sns


class HelperClass:
    nodes = 100
    edges = 11
    p = 0.7

    def __init__(self):
        sns.set_style("darkgrid")

    def plotGraph(self, graph, node_color=None, filename="", title=""):
        if node_color is not None:
            nx.draw(graph, node_color=node_color)
        else:
            nx.draw(graph)
        plt.title(title)
        plt.tight_layout()
        plt.show()
        if len(filename) > 0:
            path = "./" + filename + ".png"
            plt.savefig(path, dpi=300)
        plt.clf()
        return

    def plotHistogramm(self, bins, x_axis, filename="", x_label="bins",
                       y_label="", title=""):
        dim = 1
        if (len(bins) > 0) and (isinstance(bins[0], list)
                or isinstance(bins[0], np.ndarray)):
            dim = len(bins)
        else:
            bins = [bins]
            x_axis = [x_axis]
        plt.title(title)
        for count in range(dim):
            plt.subplot(dim, 1, count + 1)
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            plt.bar(x_axis[count], bins[count])
        plt.tight_layout()
        plt.show()
        if len(filename) > 0:
            path = "./" + filename + ".png"
            plt.savefig(path, dpi=300)
        plt.clf()
        return

    def plotInfection(self, lines, x_axis, line_label,std, filename="", x_label="Time",
                       y_label="", title=""):
        dim = 1
        if (len(lines) > 0) and (isinstance(lines[0], list)
                or isinstance(lines[0], np.ndarray)):
            dim = len(lines)
        plt.title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        for count in range(dim):
            x = np.linspace(min(x_axis), max(x_axis), len(x_axis)*2)
            y = interpolate.spline(x_axis, lines[count], x)
            std_new = interpolate.spline(x_axis, std[count], x)
            plt.plot(x, y, label=line_label[count])
            plt.fill_between(x, np.add(y, - std_new), np.add(y, std_new), alpha=0.2)
        plt.legend()
        plt.tight_layout()
        if len(filename) > 0:
            path = "./" + filename + ".png"
            plt.savefig(path, dpi=300)
        plt.show()
        #plt.clf()
        return

    def generateGraph(self, seed=0, nr_cliques=5, clique_size=20):
        graph1 = nx.connected_caveman_graph(nr_cliques, clique_size)
        graph2 = nx.powerlaw_cluster_graph(self.nodes, self.edges, self.p, seed=seed)
        nx.set_node_attributes(graph1, 'S', 'infection_type')
        nx.set_node_attributes(graph2, 'S', 'infection_type')
        return graph1, graph2

    def numberEdges(self, graph):
        sum = 0
        deg = nx.degree_histogram(graph)
        for i in range(len(deg)):
            sum += i * deg[i]
        edges = sum / 2
        return edges
