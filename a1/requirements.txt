matplotlib==3.0.3
networkx==2.2
numpy==1.16.2
scipy==1.2.1
seaborn==0.9.0
