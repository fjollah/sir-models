#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import json
from HelperClass import HelperClass
import pprint as pp
import numpy as np
import networkx as nx

# TODO Student: set student_id to your matriculation number without leading zeros
#               e.g. if your matriculation nr is 01234567 then student_id = 1234567
student_id = 11831695



def perform(graph):
    """Takes a networkx.Graph and returns the graph's the degree histogram. """

    degree_histogram = nx.degree_histogram(graph)

    # TODO Student: Calculate the graph's degree distribution.data = {}
    #for n in g.nodes():
    #if g.degree(n):
    # data[n] = float(sum(g.degree(i) for i in g[n]))/g.degree(n)
    
    return degree_histogram


if __name__ == u'__main__':
    helper = HelperClass()
    graph1, graph2 = helper.generateGraph(seed=student_id)

    histograms = [perform(graph1), perform(graph2)]   
    number_bins =  max([len(histogram) for histogram in histograms])

    # append zeros to the shorter lists
    for i in range(len(histograms)):
        histograms[i] = histograms[i] + [0] * (number_bins - len(histograms[i]))
    
    
    x_axis = [d for d in range(number_bins)]

    # want to see your graphs? comment this in!
    # helper.plotGraph(graph1)
    # helper.plotGraph(graph2)


    helper.plotHistogramm(histograms, [x_axis, x_axis], x_label="Degree", y_label="Count", title="Degree Distribution", filename="degree", align="center")




