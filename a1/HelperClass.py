import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
import seaborn as sns


class HelperClass:
    nodes = 100
    edges = 11
    p = 0.7

    def __init__(self):
        sns.set_style("darkgrid")

    def plotGraph(self, graph, node_color=None, filename="", title="", node_size=20):
        fig = plt.figure()
        ax = fig.subplots()
        fig.suptitle(title)
        if node_color is not None:
            nx.draw(graph, node_color=node_color, node_size=node_size, ax=ax)
        else:
            nx.draw(graph, node_size=node_size, ax=ax)
        fig.tight_layout()
        fig.show()
        if len(filename) > 0:
            path = "./" + filename + ".png"
            fig.savefig(path, dpi=300)
        return

    def plotHistogramm(self, bins, x_axis, filename="", x_label="bins",
                       y_label="", title="", align="edge"):
        dim = 1
        if (len(bins) > 0) and (isinstance(bins[0], list)
                or isinstance(bins[0], np.ndarray)):
            dim = len(bins)
        else:
            bins = [bins]
            x_axis = [x_axis]
        fig, axs = plt.subplots(dim, 1)
        fig.suptitle(title)
        for count in range(dim):
            bin_width = x_axis[count][1] - x_axis[count][0]
            axs[count].set_xlabel(x_label)
            axs[count].set_ylabel(y_label)
            axs[count].bar(x_axis[count], bins[count], width=bin_width, align=align)
        fig.tight_layout()

        if len(filename) > 0:
            path = "./" + filename + ".png"
            fig.savefig(path, dpi=300)
        fig.show()
        return

    def plotInfection(self, lines, x_axis, line_label,std, filename="", x_label="Time",
                       y_label="", title=""):
        dim = 1
        if (len(lines) > 0) and (isinstance(lines[0], list)
                or isinstance(lines[0], np.ndarray)):
            dim = len(lines)
        plt.title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        for count in range(dim):
            x = np.linspace(min(x_axis), max(x_axis), len(x_axis)*2)
            y = interpolate.spline(x_axis, lines[count], x)
            std_new = interpolate.spline(x_axis, std[count], x)
            plt.plot(x, y, label=line_label[count])
            plt.fill_between(x, np.add(y, - std_new), np.add(y, std_new), alpha=0.2)
        plt.legend()
        plt.tight_layout()
        plt.show()
        if len(filename) > 0:
            path = "./" + filename + ".png"
            plt.savefig(path, dpi=300)
        plt.clf()
        return

    def generateGraph(self, seed=0, nr_cliques=5, clique_size=20):
        graph1 = nx.connected_caveman_graph(nr_cliques, clique_size)
        graph2 = nx.powerlaw_cluster_graph(self.nodes, self.edges, self.p, seed=seed)
        nx.set_node_attributes(graph1, 'S', 'infection_type')
        nx.set_node_attributes(graph2, 'S', 'infection_type')
        return graph1, graph2

    def numberEdges(self, graph):
        return nx.number_of_edges(graph)
