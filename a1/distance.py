#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import json
from HelperClass import HelperClass
import pprint as pp
import numpy as np
import networkx as nx

# TODO Student: set student_id to your matriculation number without leading zeros
#               e.g. if your matriculation nr is 01234567 then student_id = 1234567
student_id=11831695


def perform(graph):
    """ Takes a networkx.Graph, calculates returns a tuple containing the graph's (normalized) distance distribution. """
  
    diameter = nx.diameter(graph)
  # # TODO Student: Calculate the graph's distance distribution.
    distances=[]
    dist_lengths = nx.shortest_path_length(graph)
    dist_dict_lengths=dict(dist_lengths)
    for source in dist_dict_lengths:
        for target, sp_length in dist_dict_lengths[source].items():
            distances.append(sp_length)

    
    distance_distribution, distance_distribution_edges = np.histogram(list(distances),bins=diameter,range=(1,6),density=True)

    
    
    return distance_distribution.tolist(), distance_distribution_edges.tolist()

if __name__ == u'__main__':
    helper = HelperClass()
    graph1, graph2 = helper.generateGraph(seed=student_id)

    x_axis = [[], []]
    bars   = [[], []]

    bars[0], x_axis[0] = perform(graph1)
    bars[1], x_axis[1] = perform(graph2)

    x_axis[0].pop()
    x_axis[1].pop()

    helper.plotGraph(graph1)
    helper.plotGraph(graph2)

    helper.plotHistogramm(bars, x_axis, filename="distance", x_label="Distance", y_label="probability", title="Distance distribution", align="center")
