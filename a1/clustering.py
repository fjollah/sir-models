#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import json
from HelperClass import HelperClass
import pprint as pp
import numpy as np
import networkx as nx


# TODO Student: set student_id to your matriculation number without leading zeros
#               e.g. if your matriculation nr is 01234567 then student_id = 1234567
student_id=11831695



def perform(graph):

   

    """Takes a networkx.Graph and returns a histogram with 10 pins of the graph's clustering coefficients.
    The returned histogram has 10 bins and a range from 0 to 1.

    Return values
    -------------
    clustering_coefficients_hist_values : list
        a list containing the values of the histogram

    clustering_coefficients_hist_edges : list
        a list containing the edges of the histogram.

      
    Notes
    -----
    The length of the returned lists is always the same because the number of bins is constant.
    Due to the fixed range and the fixed number of bins the histogram edges should always be the same.
    The return value is still required for consistency and compatibility.
    """

    # TODO Student: Calculate the graph's clustering coefficients and return a
    #               histogram with 10 bins.
    #
    # Note:
    # clustering_coefficients_hist_values
    #   - should contain the values of the histogram.
    #   - should be of type list
    #   - should have length <<number of bins>>
    #

    # clustering_coefficients_hist_edges
    #   - should contain the edges of the histogram
    #   - should be of type list

    #   - should have length <<number of bins + 1>>
    #
    # Example:
    # Performing on a graph with 200 nodes valid return values would be
    #clustering_coefficients_hist_values = [ 6,  4,  4, 15,  8,  9, 12,  3, 11,  6]
    #clustering_coefficients_hist_edges = [0. , 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. ]

    G=nx.clustering(graph)
    clustering_coefficients_bhist = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    clustering_coefficients_hist_values, clustering_coefficients_hist_edges = np.histogram(list(G.values()),bins=clustering_coefficients_bhist,range=(0.0,1.0))
    return clustering_coefficients_hist_values.tolist(), clustering_coefficients_hist_edges.tolist()


if __name__ == u'__main__':
    helper = HelperClass()
    graph1, graph2 = helper.generateGraph(seed=student_id)

    x_axis = [[], []]
    bars   = [[], []]

    bars[0], x_axis[0] = perform(graph1)
    bars[1], x_axis[1] = perform(graph2)

    x_axis[0].pop()
    x_axis[1].pop()

    helper.plotGraph(graph1)
    helper.plotGraph(graph2)

    helper.plotHistogramm(bars, x_axis, filename="clustering", title="Clustering coefficient")
    print(bars)
    print(x_axis)


