#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import json
from HelperClass import HelperClass
import pprint as pp
import numpy as np
import networkx as nx


# TODO Student: set student_id to your matriculation number without leading zeros
#               e.g. if your matriculation nr is 01234567 then student_id = 1234567
student_id=11831695

# def perform(graph):
#     degree_histogram = nx.degree_histogram(graph)
#     histograms = degree_histogram    

#     number_bins=len(histograms)
#     #number_bins =  max([len(histogram) for histogram in histograms])
#     #
#     # print(number_bins)

#     # append zeros to the shorter lists
  
#     #print(histograms)
#     x_axis = [d for d in range(number_bins)]
#     print(len(x_axis))
#     return histograms, x_axis

def perform(graph):
  degree_histogram = nx.degree_histogram(graph)
  return degree_histogram


def perform1(graph):
    
    G=nx.clustering(graph)
    clustering_coefficients_bhist = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    clustering_coefficients_hist_values, clustering_coefficients_hist_edges = np.histogram(list(G.values()),bins=clustering_coefficients_bhist,range=(0.0,1.0))
    return clustering_coefficients_hist_values.tolist(), clustering_coefficients_hist_edges.tolist()

def perform2(graph):
    
    diameter = nx.diameter(graph)
  # # TODO Student: Calculate the graph's distance distribution.
    distances=[]
    dist_lengths = nx.shortest_path_length(graph)
    dist_dict_lengths=dict(dist_lengths)
    for source in dist_dict_lengths:
        for target, sp_length in dist_dict_lengths[source].items():
            distances.append(sp_length)
    distance_distribution, distance_distribution_edges = np.histogram(list(distances),bins=diameter,range=(1,6),density=True)
    return distance_distribution.tolist(), distance_distribution_edges.tolist()





if __name__ == u'__main__':
   helper = HelperClass()
   graph1, graph2 = helper.generateGraph(seed=student_id)
   x_axis = [[], [], [], [], [], []]
   bars   = [[], [], [], [], [], []]
   
   bars[0]= perform(graph1)
   bars[1]= perform(graph2)
   for i in range(len(bars[0])):
        bars[0] = bars[0] + [0] * (len(bars[1]) - len(bars[0]))
   x_axis[0]=[d for d in range(len(bars[1]))]
   x_axis[1]=[d for d in range(len(bars[1]))]

   
   bars[2], x_axis[2] = perform2(graph1)
   bars[3], x_axis[3] = perform2(graph2)
   bars[4], x_axis[4] = perform1(graph1)
   bars[5], x_axis[5] = perform1(graph2)




   x_axis[2].pop()
   x_axis[3].pop()
   x_axis[4].pop()
   x_axis[5].pop()


   helper.plotGraph(graph1)
   helper.plotGraph(graph2)
   

   helper.plotHistogramm(bars, x_axis, filename="Plot3", title="Degree, Distance and Clustering Distribution")



